#include <stdio.h>
#include <stdlib.h>

#define PACKET struct packet
#define OK 0
#define NOMEM 1

PACKET
{
	PACKET* prox;
	char* data;
	int qtd_data;
};

int packet_init(PACKET* packet)
{
	if (packet != NULL)
	{
		packet->prox = NULL;
		packet->data = NULL;
		packet->qtd_data = 0;
		return OK;
	}
	return NOMEM;
}

void clear( PACKET* packet )
{
	while( packet->prox != NULL )
	{
		for(int i = 0; i < packet->qtd_data; i++)
		{
			*packet->data = 0;
		}
		free(packet->data);
		packet->qtd_data = 0;
		packet = packet->prox;
	}
}

int main (void)
{
	PACKET packet1;
	PACKET packet2;

	packet_init(&packet1);
	packet_init(&packet2);

	packet1.prox = &packet2;

	printf("%d\n", &packet1);
	printf("%d\n", packet1.prox);
	//printf("%d\n", *packet1.prox);
	//printf("%c\n", *packet1.data);
	printf("%d\n", &packet2);
	printf("%d\n", packet2.prox);
	//printf("%d\n", *packet2.prox);
	//printf("%c\n", *packet2.data);

	clear(&packet1);
}
