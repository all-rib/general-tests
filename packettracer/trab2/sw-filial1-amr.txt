# Switch S2

## Setting

enable
configure terminal
interface vlan1
ip address 200.200.7.66 255.255.255.224
enable secret @dmin-allan
service password-encryption
security passwords min-length 10
login block-for 180 attempts 3 within 60
ip domain-name allan.ribeiro.com.br
crypto key generate rsa
1024
line vty 0 15
login local
transport input ssh
exec-timeout 5
username allan privilege 15 password ssh@Network1ng
line console 0
exec-timeout 5
password @Cons-allan
exit
banner motd #
--------------------------------------------------------------------------
|                                                                        |
|                          Roteador Pato Branco                          |
|                                                                        |
|               ATENÇÃO Acesso Restrito a pessoas autorizadas!           |
|                                                                        |
| Administrador: Allan de Mello Ribeiro(allanribeiro@alunos.utfpr.edu.br)|
|                                                                        |
--------------------------------------------------------------------------
#
exit
copy running-config startup-config


