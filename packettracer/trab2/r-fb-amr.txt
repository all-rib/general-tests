## Router 3 - Fran. Belt.

## Setting

enable
configure terminal
hostname r-fb-amr

### Interface FA0/0

interface fa0/0
description Filial1
ip address 200.200.7.65 255.255.255.224
duplex auto
speed auto
ipv6 address 2001:DB8:ACAD:701::1/64 FE80::1
no shutdown

### Interface SE0/0/0

interface se0/0/0
description fb-ita
ip address 200.200.7.233 255.255.255.252
duplex auto
speed auto
ipv6 address 2001:DB8:ACAD:7FF::2:1/112 EUI-64
no shutdown

### Interface SE0/0/1

interface se0/0/1
description fb-ita
ip address 200.200.7.230 255.255.255.252
ipv6 address 2001:DB8:ACAD:7FF::1:2/112 EUI-64
no shutdown

### Routing table

ip route 200.200.7.96 255.255.255.224 200.200.7.234
ip route 200.200.7.240 255.255.255.252 200.200.7.234
ip route 200.200.7.236 255.255.255.252 200.200.7.234
ip route 200.200.7.224 255.255.255.252 200.200.7.234
ip route 200.200.7.0  255.255.255.192 200.200.7.234
ipv6 route 2001:DB8:ACAD:702::/64	 2001:DB8:ACAD:7FF::2:2
ipv6 route 2001:DB8:ACAD:7FF::4:0/112 2001:DB8:ACAD:7FF::2:2
ipv6 route 2001:DB8:ACAD:7FF::3:0/112 2001:DB8:ACAD:7FF::2:2
ipv6 route 2001:DB8:ACAD:7FF::/112 2001:DB8:ACAD:7FF::2:2
ipv6 route 2001:DB8:ACAD:700::/64	 2001:DB8:ACAD:7FF::2:2
ipv6 unicast-routing 
exit
copy running-config startup-config

### Security

configure terminal
enable secret @dmin-allan
service password-encryption
security passwords min-length 10
login block-for 180 attempts 3 within 60
ip domain-name allan.ribeiro.com.br
crypto key generate rsa
1024
line vty 0 15
login local
transport input ssh
exec-timeout 5
username allan privilege 15 password ssh@Network1ng
line console 0
exec-timeout 5
password @Cons-allan
exit
banner motd #
--------------------------------------------------------------------------
|                                                                        |
|                          Roteador Pato Branco                          |
|                                                                        |
|               ATENÇÃO Acesso Restrito a pessoas autorizadas!           |
|                                                                        |
| Administrador: Allan de Mello Ribeiro(allanribeiro@alunos.utfpr.edu.br)|
|                                                                        |
--------------------------------------------------------------------------
#
exit
copy running-config startup-config

# PC3

ipv4 200.200.7.67
netmask 255.255.255.224
gateway 200.200.7.65
ipv6 2001:DB8:ACAD:701::3/64
gateway 2001:DB8:ACAD:701::1/64

# PC4

ipv4 200.200.7.68
netmask 255.255.255.224
gateway 200.200.7.65 
ipv6 2001:DB8:ACAD:701::4/64
gateway 2001:DB8:ACAD:701::1/64

