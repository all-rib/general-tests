## Router 1 - pato branco

## Setting

enable
configure terminal
hostname r-pb-amr

### Interface FA0/0

interface fa0/0
description Matriz
ip address 200.200.7.1 255.255.255.192
duplex auto
speed auto
ipv6 address 2001:DB8:ACAD:700::1/64 FE80::1
no shutdown

### Interface SE0/0/0

interface se0/0/0
description pt-vit
ip address 200.200.7.225 255.255.255.252
ipv6 address 2001:DB8:ACAD:7FF::1/112 EUI-64
no shutdown

### Interface SE0/0/1

interface se0/0/1
description pb-ita
ip address 200.200.7.238 255.255.255.252
ipv6 address 2001:DB8:ACAD:7FF::3:2/112 EUI-64
no shutdown

### Routing table

ip route 200.200.7.228 255.255.255.252 200.200.7.226
ip route 200.200.7.232 255.255.255.252 200.200.7.226
ip route 200.200.7.64 255.255.255.224 200.200.7.226
ip route 200.200.7.240 255.255.255.252 200.200.7.226
ip route 200.200.7.96 255.255.255.224 200.200.7.226
ipv6 route 2001:DB8:ACAD:7FF::1:0/112 2001:DB8:ACAD:7FF::2
ipv6 route 2001:DB8:ACAD:7FF::2:0/112 2001:DB8:ACAD:7FF::2
ipv6 route 2001:DB8:ACAD:701::0/64 2001:DB8:ACAD:7FF::2
ipv6 route 2001:DB8:ACAD:7FF::4:0/112 2001:DB8:ACAD:7FF::2
ipv6 route 2001:DB8:ACAD:702::0/64 2001:DB8:ACAD:7FF::2
ipv6 unicast-routing 
exit
copy running-config startup-config

### Security

configure terminal
enable secret @dmin-allan
service password-encryption
security passwords min-length 10
login block-for 180 attempts 3 within 60
ip domain-name allan.ribeiro.com.br
crypto key generate rsa
1024
line vty 0 15
login local
transport input ssh
exec-timeout 5
username allan privilege 15 password ssh@Network1ng
line console 0
exec-timeout 5
password @Cons-allan
exit
banner motd #
--------------------------------------------------------------------------
|                                                                        |
|                          Roteador Pato Branco                          |
|                                                                        |
|               ATENÇÃO Acesso Restrito a pessoas autorizadas!           |
|                                                                        |
| Administrador: Allan de Mello Ribeiro(allanribeiro@alunos.utfpr.edu.br)|
|                                                                        |
--------------------------------------------------------------------------
#
exit
copy running-config startup-config

# PC1

ipv4 200.200.7.3
netmask 255.255.255.192
gateway 200.200.7.1
ipv6 2001:DB8:ACAD:700::3/64
gateway 2001:DB8:ACAD:700::1/64

# PC2

ipv4 200.200.7.4
netmask 255.255.255.192
gateway 200.200.7.1
ipv6 2001:DB8:ACAD:700::4/64
gateway 2001:DB8:ACAD:700::1/64


