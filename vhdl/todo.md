Circuitos combinacionais /sequenciais - simulação.
Construir um contador módulo 57 que inicia em 12 (BCD) e termina em 68 (BCD)
1) Construir em VHDL o contador baseado obrigatoriamente em contadores de 4 bits
2) Construir o testbench ( cont_57_tb) e simulá-lo no MODELSIM

Entregas:
a: "snapshot" da tela de simulação do Modelsim com o projeto "cont_57".
b: diretório (zipado) do projeto do Quartus, incluindo o TB
c: script (arquivo.do) com as diretivas de compilação e simulação

Trabalho individual


Testbench gera clock e reset


