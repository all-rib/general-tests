Library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;


--	  +--------+
--	->|clk	  q|->
--	->|rst	   |
--	->|clr	   |
--	->|enb	   |
--	->|load	   |
--	->|ld	   |
-- 	  +--------+
--
--	load: 0000
--	q: 0000
--

Entity counters is
port(	--Como é que faz ligação física? Qual o modelo de placa que a UTF tem?
	rst	: in std_logic;
	clk	: in std_logic;
	enb	: in std_logic; --enable não necessário neste exemplo
	clr	: in std_logic;
	ld	: in std_logic;
	load	: in std_logic_vector (3 downto 0));	-- internal storage
	q	: out std_logic_vector(3 downto 0);
end entity;

architecture monkiflip of counters is
	signal count: std_logic_vector (3 downto 0);   --Signal CONT: unsigned (3 downto 0);

	component divisor is 	-- Porquê é necessário um divisor? isso ajusta o clock? como?
	port(
		clk	: in std_logic;
		rst	: in std_logic;
		div50	: out std_logic
	); End component; --1 pulso a cada 500k


	signal clock_1cs : std_logic; --Clock 1 centésimo?
	Begin

		div_1centesimo : divisor --Que isso?

		port map(
			clc => clk,
			rst => rst,
			div50 => clock_1cs
		);
		Process (clk, rst)
		Begin
			If rst = '1' then
				load <= "0000";
				count <= "0000";
			Elsif clk'event and clk = '1' then
				If clock_1cs = '1' then
					If clr = '1' then
						count <= "0000";
					Else
						count <= load;
						If ld = '1' then
							count <= load;
						End IF;
					End If;
				End If;
			Elsif clk'event
				If clock_1cs = '1' then
					If ld = '1' then --Tem um jeito bom de guardar o count?
						load <= count;
					End IF;
				End If;

			End if;
		End process;
	q <= count;
End architecture;


