ENTITY and_port IS
   PORT ( a, b : IN BIT;
          sum, carry : OUT BIT);
END and_port;
ARCHITECTURE and_port_arch OF and_port IS
     BEGIN
          sum <= a xor b;
          carry <= a and b;
END and_port_arch;