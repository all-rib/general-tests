#include <msp430g2553.h>

unsigned int stepper = 0;
unsigned int step = 1000; //With 30 steps, it's 110 for each step (ceil)
unsigned int total = 32767; //Begin at medium velocity
unsigned int inf = 4001;
unsigned int sup = 61534;

void init_clock(void)
{
	DCOCTL = CALDCO_1MHZ;
	BCSCTL1 = CALBC1_1MHZ;
	//BCSCTL2 = DIVS0 + DIVS1;
	BCSCTL3 = XCAP0 + XCAP1;

	while(BCSCTL3 & LFXT1OF);
	__enable_interrupt();
}

/* Responsible for reading any variations in the encoder, and works as a debouncer */
void init_timer0(void)
{
	TA0CTL = TASSEL1;
	TA0CCTL0 = CCIE;
	TA0CCR0 = 1999; //Count 25ms
}

/* Interrupts in variable time t, which will change the motor pass (pwm) */
void init_timer1(void)
{
//Considering 1MHz clock
// At minimum speed, the stepper steps each 65.535 ms. At maximum, 0.7ms
	TA1CTL = TASSEL1 + MC0;
	TA1CCTL0 = CCIE;
	TA1CCR0 = total; //Begin at medium velocity
}

void init_port1(void)
{
	P1DIR = ~(BIT0 + BIT1); //Pins 0 and 1 as input, the rest as output
	P1REN = BIT0 + BIT1; //Enable pull up/pull down resistor on pin 0
	P1OUT = BIT0 + BIT1; //Put the pin in Pull-Down, and other ports as low
	P1IES = BIT0;
	P1IFG = 0;
	P1IE = BIT0; //Enable interrupt for pin 0
}

void init_port2(void)
{
	P2DIR = 0xff;
	P2OUT = 0;
}

__attribute__ ((interrupt (TIMER1_A0_VECTOR)))
void timer1_interrupt(void)
{
	stepper = stepper % 4 + 1;

	switch (stepper)
	{
		default:
			P2OUT = BIT0;
			break;
		case 2:
			P2OUT = BIT1;
			break;
		case 3:
			P2OUT = BIT2;
			break;
		case 4:
			P2OUT = BIT3;
			break;
	}
}

__attribute__ ((interrupt (PORT1_VECTOR)))
void port1_interrupt(void)
{
	P1IE = 0;
	TA0CTL |= MC0; //Start the debounce timer
}

__attribute__ ((interrupt (TIMER0_A0_VECTOR)))
void timer0_interrupt(void)
{
	TA0CTL &= ~MC0; //Stop counting the debounce timer
	if((~P1IN) && BIT0) //If port 1 is up, then its CW, rise 
	{
		if (P1IN && BIT1) //If port 1 is up, then its CW, rise 
			total -= step;
		else //Its CCW, diminish
			total += step;
		if(total < inf) //When comes to zero, limit to minimal
			total = inf;
		if(total > sup) //If at maximum, limit to almost stop
			total = sup;
		TA1CCR0 = total;
	}
	P1IFG = 0;
	P1IE |= BIT0;
}

int main(void)
{
	WDTCTL = WDTPW | WDTHOLD;

	init_clock();
	init_timer0();
	init_timer1();
	init_port1();
	init_port2();

	while (1)
	{
	}
	return 0;
}
