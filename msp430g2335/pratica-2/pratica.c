#include <msp430g2553.h>

#define DEBOUNCE 25 //Time in ms

int c, d, s, m; //Cent, dec, sec and minutes
int initial_time, final_time, counted = 0;

void init_clock(void)
{
	DCOCTL = CALDCO_1MHZ;
	BCSCTL1 = CALBC1_1MHZ;
	//BCSCTL2 = DIVS0 + DIVS1;
	BCSCTL3 = XCAP0 + XCAP1;

	while(BCSCTL3 & LFXT1OF);
	__enable_interrupt();
}

//Debouncer timer
void init_timer1(void)
{
	TA1CTL = TASSEL1;
	TA1CCTL0 = CCIE;
	TA1CCR0 = DEBOUNCE*1000-1; //Considering 1MHz clock
}

void init_timer0(void)
{
	TA0CTL = TASSEL1;
	TA0CCTL0 = CCIE;
	TA0CCR0 = 9999; //The minimal count is 1 centesimal of a second
}

void init_port1(void)
{//ZERO IS INPUT, DAMMIT
	P1SEL |= BIT1+BIT2; //Activate TA0.CCI0A and TA0.CCI1A (not sure how this works)
	P1DIR &= ~BIT3; //Key S2(P1.3) as input
	P1REN |= BIT3; //Enable pull up/pull down resistor on pin 3
	P1OUT = BIT3; //Put the pin in Pull-Up, and other ports as low
	P1IES = 1;
	P1IFG = 0;
	P1IE = BIT3; //Enable interrupt for pin 3
}

//S2 Key interrupt
__attribute__ ((interrupt (PORT1_VECTOR)))
void port1_interrupt(void)
{
	TA1CTL |= MC0;
	P1IFG = 0;
	TA1R = 0;
	//TA0CCTL0 += CAP+SCS+CCIS_3+CM_3; //Init capture mode Timer 0, gather both rising and falling edges
	//Is this right?
}

__attribute__ ((interrupt (TIMER0_A0_VECTOR)))
void timer0_interrupt(void)
{
	c++;
	if(c > 9)
	{
		c = c-10;
		d++;
		if(d > 9)
		{
			d = d-10;
			s++;
			if(s > 59)
			{
				s = s-60;
				m++;
			}
		}
	}
}

void set_timer0(void)
{
	c = d = s = m = 0;
	initial_time = TA0CCR0;
	TA0CTL |= MC0;
}

void reset_timer0()
{
	final_time = TA0CCR0+initial_time; //The last cycle must be added into final time count
	while (final_time > 9999)
	{
		final_time = final_time - 10000;
		c++;
		if(c > 9)
			d++;
		if(d > 99)
			s++;
		if(s > 59)
			m++;
		if(m > 59)
			m = 0;
	}
	TA0CTL &= ~MC0;
	counted = 0;
}

__attribute__ ((interrupt(TIMER1_A0_VECTOR)))
void timer1_interrupt(void)
{
	TA1CTL &= ~MC0; //Stop counting the debounce timer
	if(P1IN && BIT3) // The button pin must not enter timer (how i do this?)
	{
		P1IFG = 0;
		if(counted) //If finished counting, reset the timer
		{
			reset_timer0();	//Stop the timer, and add time
		}
		else
		{
			set_timer0(); //Start counting, and add time;
			counted = 1; //The next button interrupt will cause timer to stop
		}
	}
}

int main(void)
{
	WDTCTL = WDTPW | WDTHOLD;

	init_clock();
	init_timer0();
	init_timer1();
	init_port1();

	while (1)
	{

	}
	return 0;
}
