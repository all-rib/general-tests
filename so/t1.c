/* Escreva um programa que realiza tratamento de sinais POSIX. O programa deve:
 *
 *	1 - Capturar os sinais SIGINT, SIGTSTP e SIGPIPE mudando suas operações
 *	para
 *	
 *		O sinal SIGINT deve ser ignorado
 *	
 *		Contar quantas vezes o sinal SIGTSTP é gerado pelo usuário
 *	
 *		O SIGPIPE deve zerar o contador do SIGTSTP
 *	
 *	2 - Utilize um for(EVER) para manter o processo vivo enquanto gera
 *	sinais
 *
 * 	3 - Gerar os seguintes sinais de teste
 *	
 *		CTRL+C, no terminal (número indefinido)
 *	
 *		SIGPIPE pelo comando kill no terminal
 *	
 *		SIGTSTP a cada 10 segundo
 *
 *	4 - Se o contador de SIGTSTP chegar a 12, o processo deve gerar um
 *	sinal a ele mesmo cuja ação default seja seu término
 *
 *
 *
 * man 7 signal
 *
 *	Each signal has a current "disposition", which determines how the proccess behaves
 *
 *	Term	Default action is to terminate the proccess.
 *
 *	Ign	Default action is to ignore the signal.
 *
 *	Core	Default action is to terminate the proccess and dump core (see core(5))
 *
 *	Stop	Default action is to stop the proccess.
 *
 *	Cont	Default action is to continue the proccess if it is currently stopped.
 *
 *
 *	A proccess can change the disposition of a signal using sigaction(2) or signal(2)
 *		Note: The latter is less portable when establishing a signal handler
 *
 *	Question: Is it possible to avoid a signal changing the signal's stack? man sigalt-stack(2)
 *
 * man 2 sigaction()
 *
 *	Standard signals
 *		Signal		Standard	Action		Comment
 *		----------------------------------------------------------------------------------
 *		SIGINT		P1990		Term		Interrupt from keyboard
 *		SIGTSTP		P1990		Stop		Stop typed at terminal
 *		SIGPIPE		P1990		Term		Broken pipe: write to pipe with no
 *
 *	Signal numbering for standard signals
 *		Signal		x86/ARM		Alpha/SPARC	MIPS		PARISC
 *		----------------------------------------------------------------------------------
 *		SIGINT	  	2		2		2		2
 *		SIGPIPE		13		13		13		13
 *		SIGTSTP		20		18		24		25
 */


#ifdef PRINT
	#include <stdio.h>
#endif

#include <stdlib.h>
#include <signal.h>
#include <sys/types.h>
#include <unistd.h>
#include <time.h>

int count = 0;

void SIGINT_handler(int signal)
{
	//Must be ignored
}

void SIGTSTP_handler(int signal)
{
	//Counts each 10s
	//Count how many calls are made
	//If reaches 12, kill proccess
	if(count == 12)
	{
		exit(0);
	}
	count++;
}

void SIGPIPE_handler(int signal)
{
	//Reaches from kill
	//Must set SIGTSTP count to zero
	count = 0;
	#ifdef PRINT
		printf("counter set to zero\n");
	#endif
}
	
//#define SAVE_DFL_SGN

int main(void)
{
	#ifdef PRINT
		printf("PID %d\n", getpid());
	#endif
	#ifdef SIGACTION
		struct sigaction * sigint = (struct sigaction *)malloc(sizeof(struct sigaction));
		sigint->sa_handler = SIGINT_handler;
		struct sigaction * sigtstp = (struct sigaction *)malloc(sizeof(struct sigaction));
		sigtstp->sa_handler = SIGTSTP_handler;
		struct sigaction * sigpipe = (struct sigaction *)malloc(sizeof(struct sigaction));
		sigpipe->sa_handler = SIGPIPE_handler;

		#if SAVE_DFL_SGN
			struct sigaction * sigint_dfl = (struct sigaction *)malloc(sizeof(struct sigaction));
			struct sigaction * sigpipe_dfl = (struct sigaction *)malloc(sizeof(struct sigaction));
			struct sigaction * sigtstp_dfl = (struct sigaction *)malloc(sizeof(struct sigaction));
			sigaction(SIGINT, sigint, sigint_dfl);
			sigaction(SIGTSTP, sigtstp, sigtstp_dfl);
			sigaction(SIGPIPE, sigpipe, sigpipe_dfl);
		#else
			sigaction(SIGINT, sigint, (struct sigaction *)NULL);
			sigaction(SIGPIPE, sigpipe, (struct sigaction *)NULL);
			sigaction(SIGTSTP, sigtstp, (struct sigaction *)NULL);
		#endif
	#else
		signal(SIGINT, (void(*)(int))SIGINT_handler);
		signal(SIGTSTP, (void(*)(int))SIGTSTP_handler);
		signal(SIGPIPE, (void(*)(int))SIGPIPE_handler);
	#endif

	float ini = (clock()/CLOCKS_PER_SEC);
	while(1)
	{
		while((float)clock()/CLOCKS_PER_SEC - ini < 10);
		ini = (clock()/CLOCKS_PER_SEC);
		raise(SIGTSTP);
		#ifdef PRINT
			printf("counter: %d\n", count);
		#endif
	}
	return (0);
}
