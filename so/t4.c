/* Faca um programa que imprima os nros primos existentes entre 0 e 99999.
 *
 *	O programa deve dividir o espaço de cálculo uniformemente entre as
 *	N threads da forma que vc achar mais efetivo do ponto de vista
 *	computacional. Ex: 1bilhão de termos com 2 threads = 500milhões de
 *	termos em cada thread;
 *
 *	cada thread efetua uma soma parcial de forma autônoma;
 *
 *	para evitar o uso de mecanismos de sincronização, cada thread T[i]
 *	deve depositar seu resultado parcial na posição result[i] de um
 *	vetor de resultados parciais.
 *
 *	após o término das threads de cálculo, o programa principal soma os
 *	números primos encontrados
 *
 *	execute as soluções com N = {1, 2, 4, 8 e 16} threads
 *
 *	Marque o tempo necessário para calcular Pi para cada N e faça um
 *	gráﬁco de linhas (NxTempo) apresentado os resultados.
 *
 *	Os 2 que conseguirem o melhor resultado de tempo ganharão 1 ponto
 *	extra na prova 1
 */

int main(void)
{
	return (0);
}
