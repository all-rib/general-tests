/* Faça um programa em que três processos executam paralelamente as
 * seguintes ações:
 *
 *	Pai - Imprime os números de 1 a 50, com um intervalo de 2 segundos
 *	entre cada número. Após imprimir todos os números, imprime a frase
 *	“Processo pai vai morrer”.
 *
 *	Filho1 - Imprime os números de 100 a 200, com um intervalo de 1
 *	segundo entre cada número. Antes de imprimir os números, imprime a
 *	frase “Filho 1 foi criado”. Após imprimir todos os números, imprime
 *	a frase “Filho 1 vai morrer”.
 *
 *	Filho 2 - Inicia apeas após o pai morrer, imprime os números de 51
 *	a 99. Importante, em cada printf os processos devem imprimir o seu
 *	pid e o pid do seu pai.
 *
 */

void print(char * string)
{
	printf("%d %d\n");
	printf("%s\n", string);
}

char filho1(void)
{
	print("Filho 1 foi criado\n");
	for(char i = 100; i < 201; i++)
	{
		printf("%d\n", i);
	}
	print("Filho 1 vai morrer\n");
}

char filho2(void)
{
	for(char i = 51; i < 100; i++)
	{
		printf("%d\n", i);
		sleep(tsleep);	
	}
	if(call == 1)
	{
		
	}
}

int main(void)
{
	for (int i = 1; i < 51; i++)
	{
		printf("%d\n", i);
		sleep(2);	
	}
	printf("Processo pai vai morrer\n", i);
	return(0);
}
