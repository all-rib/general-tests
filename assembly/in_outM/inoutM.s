/*
Este programa é semelhante ao in_out, exceto pelo fato de ser um switch com memória:
Você aperta, e ele acende. Você aperta de novo, e ele apaga. Fica alternando.
*/

#include <avr/io.h>

.set PINB, 0x03
.set DDRB, 0x04
.set PORTB, 0x05
.set PINC, 0x06
.set DDRC, 0x07
.set PORTC, 0x08

.set PSTATE, 0b01		;//O estado das portas: porta PB1 como entrada (botão), PB0 como saída(led).

.org 0x00
	out DDRB, PSTATE	;//Configurando entrada e saída de dados.
	nop			;//A mudança de estado entrada/saída das portas leva um tempo. Vamos esperar
	rcall loop

loop:
	rcall read
	rjmp loop

read:
	nop
	in r16, PINB		;//Lemos o estado de PINBX, e escrevemos no r16 (r0-r15 não fazem operaçoes diretas)
	sbrc r16, 1
	rcall write		;//Só chamamos a write quando o botão for apertado.
/*
BRBS faz um branch(ramificação, ou chamado de função) caso um detereminado bit esteja setado (1). Ou seja, apenas escreve
o estado do led caso o botão seja apertado.
*/
ret

write:
	com r17
	andi r17, 0b100000
	out PORTB, r17
/*
Uma traquinagem AINDA MAIOR aqui. Para evitar if's, criei uma operação que inverte os bits do registro toda vez que a
função é chamada: dado uma série de bits, aplicamos a operação com(not, ou complemento), invertendo o estado dos bits,
seguido por um andi para selecionar o estado de um bit específico: Por exemplo, inverter bit 0:
0b0000 ->(not)-> 0b1111 ->(andi 0b0001)-> 0b0001 (ligou)
0b0001 ->(not)-> 0b1110 ->(andi 0b0001)-> 0b0000 (desligou)
no caso, o análogo ao not para esse micro é com (complemento de bits).
*/
ret
