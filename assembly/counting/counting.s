/*
/*Contagem de 15 até zero, piscando o led, para testes de memória
/*
/*
/*
*/
#include <avr/io.h>


;.set PINB,	0x03
.set DDRB,	0x04	;É a única porta que importa, pois queremos o led da placa.
.set PORTB,	0x05
;.set PINC,	0x06
;.set DDRC,	0x07
;.set PORTC,	0x08
;.set PIND,	0x09
;.set DDRD,	0x0A
;.set PORTD,	0x0B

.org 0x00000000

	ldi r16, 0x0F	;Piscar 16 vezes.
	sbi DDRB, 5	;Configurar o led como pino de saída
	rcall contador	;Chamar o contador, para piscar o led
	;cbi PORTB, 5	;Desligar o led
	rcall infinity	;Entrar em loop infinito, para não ligar de novo

infinity:
	rcall infinity

contador:
	conta:
		rcall piscar	;Piscar acende o led, espera 1 segundo, e apaga.
		dec r16		;Descrescer 1 do registro, para contar uma piscada
		brne conta	;Fazer o loop até que r16 chegue à zero
ret

piscar:
	rcall delay_1s	;O delay de 1 segundo é um wrapper de outras funções
	sbi PORTB, 5	;Liga o led
	rcall delay_1s	;Espera mais 1 segundo
	cbi PORTB, 5	;Desliga o led
ret

delay_1s:
	rcall delay_500ms
	rcall delay_500ms
ret
delay_500ms:
	rcall delay_100ms
	rcall delay_100ms
	rcall delay_100ms
	rcall delay_100ms
	rcall delay_100ms
ret
delay_100ms:
	rcall delay_20ms
	rcall delay_20ms
	rcall delay_20ms
	rcall delay_20ms
	rcall delay_20ms
ret
delay_20ms:
	rcall delay_5ms
	rcall delay_5ms
	rcall delay_5ms
	rcall delay_5ms
ret
delay_5ms:
	rcall delay_1ms
	rcall delay_1ms
	rcall delay_1ms
	rcall delay_1ms
	rcall delay_1ms
ret
delay_1ms:
	rcall delay_1us
	rcall delay_1us
	rcall delay_1us
	rcall delay_1us
	rcall delay_1us
	rcall delay_1us
	rcall delay_1us
	rcall delay_1us
	rcall delay_1us
	rcall delay_1us
ret
delay_1us:
	ldi r17, 99	;Contar 1 microssegundo, usando 100 ciclos.
	delay_1us1:
		nop
		nop
		nop
		nop
		nop
		dec r17
		brne delay_1us1
ret
