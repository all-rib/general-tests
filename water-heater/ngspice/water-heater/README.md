offset:
Entrada: qualquer simétrica de até 6.75VPP
Saída: A mesma simétrica, mas exclusivamente positiva

Comparador:
Entrada: em -, tensão de controle
em +, simétrica
Saída: PWM controlado pela tensão de controle

Sub:
Entrada: em -, o valor a ser subtraído(B)
em +, função da qual deseja-se subtrair um valor (A)
Saída: A-B;
Atente-se que, no pico de A, |A-B| deve ser menor que VSAT

Gain:
Entrada: Tensão qualquer
Saída: A tensão V aumentada em 10x
Atente-se que 10*V <= VSAT
