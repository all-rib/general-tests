#!/bin/bash

# This scripts gathers all my circuits toghether, commenting the code as it
# passes through

parser()
{

	# TODO: create a flag to choose parts of commentaries to be
	# included. Head only, for example
	# TODO: Shift $1 so i may pass more than one file at the same time
	lib=$1
	if ! [ -a $lib/$lib.lib ] && ! [ -a $lib.lib ]; then
			echo -e "$lib: Lib not found";
	else
		if [ -a $i/$i.lib ]; then
			echo -e "Adding lib $i";
			cat $i/$i.lib >> main.lib;
		else
			cat $i.lib >> main.lib;
		fi
	fi
}

options()
{
	# TODO: add an option wether to recompile, remove, or verify the
	# program
	echo -e "I can't do anything, yet...";
}

compile()
{
	if [ -z $@ ]; then
		rm -rf main.lib;
		for i in *; do
			if [ -d $i ]; then
				parser $i;
			fi
		done
	else
		options $@;
	fi

}

simulate()
{
	echo -e "Simulating $1\n";
	ngspice -b $1;
}

run()
{
	if [ $1 == macro ]; then
		compile
		cat system.cir > main.cir
		cat macro.cir >> main.cir
		simulate main.cir;
	elif [ $1 == micro ]; then 
		compile
		cat system.cir > main.cir
		cat micro.cir >> main.cir
		simulate main.cir;
	else
		rm -r images/exp-*;
		for i in *; do
			if [ -d $i ] && [ -a $i/*.cir ]; then
				cd $i;
				simulate "*$i.cir"
				cd ..;
			fi
		done
	fi
}

main()
{
	while [ -n "$1" ]; do
		case "$1" in
			--macro)
				run macro;
			;;
			--micro)
				run micro;
			;;
			--test)
				run tests;
			;;
			--view)
				gwen=1;
			;;
			*)
				echo -e "$1 is not an option";
				exit
			;;
		esac
		shift
	done
	if [ -n $gwen ]; then
		gwenview images/
	fi
}
main $@
