MOSFET COMO CHAVE E CIRCUITO DE DRIVER COM OPTOACOPLADOR

**************************************************************************
* Nome: Allan Ribeiro; RA: 2037807
* Programa utilizado: ngspice-35
**************************************************************************
* Indice:
*	1 - Descrição do problema
*	2 - Modelagem do optoacoplador
*	3 - Modelagem do mosfet
*	4 - Outros comentários
*	5 - Modelagem da fonte
*	6 - Inclusão de bibliotecas
*	7 - Descrição do circuito
*	8 - Simulação
*	9 - Referências
*
* Lista de arquivos:
*	rec2.cir - Presente arquivo. Registro de estudo sobre LED's;
*	D.lib - Biblioteca contendo modelos diversos de diodos;
*	1-1.png - Comparação V1 X Vopt, para 500Hz
*	1-2.png - Gráfico de ruído GND
*	1-3.png - Comparação V1 X Vg, para 500Hz, sem R4
*	1-4.png - Comparação V1 X Vg, para 500Hz, com R4
*	1-5.png - Comparação V1 X Vd para 500Hz
*	2-1.png - Comparação V1 X Vg, para 5kHz, 
*	2-2.png - Comparação V1 X Vds, para 5kHz
*	3-1.png - Comparação V1 X Vgs, 20kHz
*	3-2.png - Comparação V1 X Vds, 20kHz
*	4-1.png - Comparação V1 X Vds, 30kHz
*	5-1.png - Comparação V1 X Vds, 37kHz
*
**************************************************************************
* 1 - Descrição do problema
**************************************************************************
* Visualizar arquivo 2.pdf
*
**************************************************************************
* 2 - Modelagem do optoacoplador
**************************************************************************
* O exemplo trabalha com o CI 4N25, cujo modelo está disponível na
* internet[2], e é compatível com o ngSPICE. O modelo presente foi obtido
* do arquivo modelos_subckt/spice_complete/OPTO.LIB
*
* Outro detalhe trata da pinagem: como o modelo apresentado possui 6
* entradas para o CI, e um deles nunca é usado, o subcircuito foi
* rearranjado. Atenção aos pinos base, coletor e emissor.
* 
***************************************************************************
* 3 - Modelagem do mosfet
***************************************************************************
* O exemplo trabalha com o IRF740, cujo modelo está disponível na
* internet[3], e é compatível com o ngSPICE. Existem dois modelos
* propostos: a versão de subcircuito
* (modelos_subckt/spice_complete/POWMOS.LIB), e a versão NMOS, que é a
* utilizada. O modelo pronto permite a possibilidade de usar também o pino
* de substrato, que aterrado por razões óbvias.
* 
* Importante notar que o modelo usado, apesar de utilizar 0 como valor de
* dopagem (NSUB) padrão, não é a mesma usada pelo programa, que utilizará o menor valor
* possível, mas nunca zero, ainda que apresente tal valor quando monitorado.
*
***************************************************************************
* 4 - Modelagem da Fonte
**************************************************************************
* A fonte V1 é um pulso, formato quadrado, com 30% de ciclo de trabalho,
* 3.3V de pico a pico, logo, varia de 0V à 3.3V. A frequência
* fornecida (500Hz) implica uma peridiocidade de 2e-3s, logo uma largura de
* pulso de 30%, mas há de se alterar conforme problema proposto.
* 
* PW = 0.3*2e-3 = 6e-4s
* 
* Quanto à fonte VCC, o valor é dado, a saber 12V.
* 
**************************************************************************
* 5 - Outros comentários
**************************************************************************
* O circuito do problema faz uso de dois GND's, dando a entender que
* existe diferença entre os potenciais utilizados em cada um,
* respectivamente. Usar o mesmo gnd em ambos provoca ruído nos sinais,
* então para simular o comportamento de um GND, hei de subtrair o potencial
* do nó 8, que seria o segundo gnd, de todos os valores relativos a esta
* parte do circuito. Como o potencial nesse ponto não é nulo, o ruído
* gerado por ele, se subtraído dos sinais ruidosos, deve corrigir estes
* mesmos sinais, isto é, para todos os n nós com n>2. Importante notar, que
* este mesmo ruído é abstraído pelo sinal do Source do MOSFET. Portanto, em
* algumas ocasiões, o mesmo valor será subtraído duas vezes de alguma
* medida.
* 
* Também é importante citar que não faz diferença o circuito possuir duas
* fontes VCC independentes, ou apenas VCC, alimentanto dois outros nós.
* 
**************************************************************************
* 6 - Bibliotecas
**************************************************************************
* Biblioteca contendo o modelo do MOSFET e Optoacoplador
.include M.lib

**************************************************************************
* 7 - Descrição do circuito
**************************************************************************
* Fonte quadrada, 3.3V, 30% (Ground 1)
* 500Hz
*V1 1 0 pulse(0 3.3 0 0 0 6e-4 2e-3 0)

* 5kHz
*V1 1 0 pulse(0 3.3 0 0 0 6e-5 2e-4 0)

* 20kHz
*V1 1 0 pulse(0 3.3 0 0 0 1.5e-5 5e-5 0)

* 30kHz
*V1 1 0 pulse(0 3.3 0 0 0 1e-5 3.33e-5 0)

* 37kHz
V1 1 0 pulse(0 3.3 0 0 0 8.12e-6 2.7e-5 0)

* Fontes VCC
VCC1 4 8 12
VCC2 7 8 12 

* Resistores
R1 1 2 220

R2 4 3 3252

R3 5 3 10.1

R4 7 6 151.2

* Optoacoplador, 1 2 5 6 4, respectivamente
X1 2 0 3 0 8 4N25

* Pinagem usada no problema: 2 gnd 3 float gnd2

* MOSFET, D, G, S, e Substract respectivamente
M1 6 5 8 8 IRF740

* Resistor opcional para conectar ambos os GND's
*R5 0 8 1e-14

**************************************************************************
* 8 - Simulação
**************************************************************************
* Análise no tempo, a cada 1ms, de 0 à 100ms
* 500Hz
*.TRAN 0.1us 8ms

* 5kHz
*.TRAN 1ns 200us

* 20kHz
*.TRAN 1ns 120us

* 30kHz
*.TRAN 1ns 1.4e-4s

* 37kHz
.TRAN 1ns 80us

.control

run

* Taxa constante de tempo
*linearize

* Teste do aterramento secundário

let V1 = V(1)
let VGND = V(8)
let VOPT = V(3)-VGND
let VG = V(5)
let VD = V(6)
let VS = V(8)
let Vgs = VG-VS-VGND
let Vds = VD-VS-VGND

set set gnuplot_terminal=png/quit

gnuplot 3-2 V1 VD-VS-VGND
+ title 'Comparison between MOSFET Vds and Signal generator, 20kHz'
+ xlabel 'Time/s'
+ ylabel 'Voltage/V'

gnuplot 3-1 V1 VG-VS-VGND
+ title 'Comparison between MOSFET Vds and Signal generator, 20kHz'
+ xlabel 'Time/s'
+ ylabel 'Voltage/V'


.endc

.end

**************************************************************************
* 9 - Referências
**************************************************************************
* Manual do ngSPICE
* [1] - http://ngspice.sourceforge.net/docs/ngspice-manual.pdf
* 
* Modelo do 4N25, Modelo do IRF640
* [2,3] - http://ngspice.sourceforge.net/modelparams.html
* 
**************************************************************************
* Universidade Tecnológica Federal do Paraná - Pato Branco, 2021
**************************************************************************
