# Atividade recuperação

Nome: Allan Ribeiro; RA: 2037807
Programa utilizado: ngspice-35

### Indice:

	1 - Descrição do problema
	2 - Modelagem do optoacoplador
	3 - Modelagem do mosfet
	4 - Outros comentários
	5 - Modelagem da fonte
	6 - Inclusão de bibliotecas
	7 - Descrição do circuito
	8 - Simulação
	9 - Conclusão
	10 - Referências

### Lista de arquivos:

	rec2.cir - Presente arquivo. Registro de estudo sobre LED's;
	D.lib - Biblioteca contendo modelos diversos de diodos;
	1-1.png - Comparação V1 X Vopt, para 500Hz
	1-2.png - Gráfico de ruído GND
	1-3.png - Comparação V1 X Vg, para 500Hz, sem R4
	1-4.png - Comparação V1 X Vg, para 500Hz, com R4
	1-5.png - Comparação V1 X Vd para 500Hz
	2-1.png - Comparação V1 X Vg, para 5kHz, 
	2-2.png - Comparação V1 X Vds, para 5kHz
	3-1.png - Comparação V1 X Vgs, 20kHz
	3-2.png - Comparação V1 X Vds, 20kHz
	4-1.png - Comparação V1 X Vgs, 30kHz
	4-2.png - Comparação V1 X Vds, 30kHz
	5-1.png - Comparação V1 X Vds, 37kHz

### 1 - Descrição do problema

Visualizar arquivo ![2.pdf](2.pdf)

### 2 - Modelagem do optoacoplador

O exemplo trabalha com o CI 4N25, cujo modelo está disponível na
internet[2], e é compatível com o ngSPICE. O modelo presente foi obtido
de (modelos_subckt/spice_complete/OPTO.LIB);

Outro detalhe trata da pinagem: como o modelo apresentado possui 6
entradas para o CI, e um deles nunca é usado, o subcircuito foi
rearranjado. Atenção aos pinos base, coletor e emissor.
 
### 3 - Modelagem do mosfet

O exemplo trabalha com o IRF740, cujo modelo está disponível na
internet[3], e é compatível com o ngSPICE. Existem dois modelos
propostos: a versão de subcircuito
(modelos_subckt/spice_complete/POWMOS.LIB), e a versão NMOS, que é a
utilizada. O modelo pronto permite a possibilidade de usar também o pino
de substrato, que aterrado por razões óbvias.
 
Importante notar que o modelo usado, apesar de utilizar 0 como valor de
dopagem (NSUB) padrão, não é a mesma usada pelo programa, que utilizará o menor valor
possível, mas nunca zero, ainda que apresente tal valor quando monitorado.

### 4 - Modelagem da Fonte

A fonte V1 é um pulso, formato quadrado, com 30% de ciclo de trabalho,
3.3V de pico a pico, logo, varia de 0V à 3.3V. A frequência
fornecida (500Hz) implica uma peridiocidade de 2e-3s, logo uma largura de
pulso de 30%, mas há de se alterar conforme problema proposto.
 
	PW = 0.3*2e-3 = 6e-4s
 
Quanto à fonte VCC, o valor é dado, a saber 12V.

### 5 - Outros comentários

O circuito do problema faz uso de dois GND's, dando a entender que
existe diferença entre os potenciais utilizados em cada um,
respectivamente. Usar o mesmo gnd em ambos provoca ruído nos sinais,
então para simular o comportamento de um GND, hei de subtrair o potencial
do nó 8, que seria o segundo gnd, de todos os valores relativos a esta
parte do circuito. Como o potencial nesse ponto não é nulo, o ruído
gerado por ele, se subtraído dos sinais ruidosos, deve corrigir estes
mesmos sinais, isto é, para todos os n nós com n>2. Importante notar, que
este mesmo ruído é abstraído pelo sinal do Source do MOSFET. Portanto, em
algumas ocasiões, o mesmo valor será subtraído duas vezes de alguma
medida.

Também é importante citar que não faz diferença o circuito possuir duas
fontes VCC independentes, ou apenas VCC, alimentanto dois outros nós.
 
### 6 - Bibliotecas

Biblioteca contendo o modelo do MOSFET e Optoacoplador

	.include M.lib

### 7 - Descrição do circuito

![circuito](imagens/circuito.png)

### 8 - Simulação

<details>
<summary><b>Análise de tensões em 500Hz, com variação de resistência</b></summary>

![Comparação V1 X Vopt, para 500Hz](imagens/1-1.png)

![Gráfico de ruído GND](imagens/1-2.png)

![Comparação V1 X Vg, para 500Hz, sem R4](imagens/1-3.png)

![Comparação V1 X Vg, para 500Hz, com R4](imagens/1-4.png)

![Comparação V1 X Vd, para 500Hz](imagens/1-5.png)

</details>

<details>
<summary><b>Análise de tensões com frequência em 5kHz</b></summary>

![Comparação V1 X Vgs, para 5kHz](imagens/2-1.png)

![Comparação V1 X Vds, para 5kHz](imagens/2-2.png)

</details>
<details>
<summary><b>Análise de tensões com frequência em 20kHz</b></summary>

![Comparação V1 X Vgs, para 20kHz](imagens/3-1.png)

![Comparação V1 X Vds, para 20kHz](imagens/3-2.png)

</details>

<details>
<summary><b>Análise de tensões com frequência em 30kHz</b></summary>

![Comparação V1 X Vgs, para 30kHz](imagens/4-1.png)

![Comparação V1 X Vds, para 30kHz](imagens/4-2.png)

</details>

<details>
<summary><b>Análise de tensões com frequência em 37kHz</b></summary>

![Comparação V1 X Vds, para 37kHz](imagens/5-1.png)

</details>

### 9 - Conclusão

A primeira diferença entre os gráficos para com o resultado proposto é
claro: as ondas complementares à fonte não foram deslocadas em valor,
portanto não se sobrepõem, mas o valor para 1-1, 1-3 e 1-4 permanecem o mesmo, 12V.

O primeiro gráfico distoante é o 1-5. Em primeiro
instante tratei de rever os modelos utilizados, mas na falta de
conhecimento teórico sobre o cálculo das constantes, resolvi procurar por
outros modelos. Todos apresentaram o mesmo comportamento.

A segunda tentativa se deu em rever o circuito, e especulo que o problema
surge graças à diferença de aterramento usado na simulação para com o do
projeto original. Contudo, numa escala menor percebe-se que o sinal é semelhante, só não de mesma amplitude.

O mesmo resultado se repete com os gráficos seguintes, reduzindo a
amplitude em mesma escala em comparação à do projeto original, em especial
para 37kHz de frequência. Creio que com aterramento correto observaríamos
a falha de operação do mosfet para 37kHz, que não abriria circuito, como
proposto no princípio.

### 10 - Referências

Manual do ngSPICE

[1](http://ngspice.sourceforge.net/docs/ngspice-manual.pdf)

Modelo do 4N25, Modelo do IRF640

[2,3](http://ngspice.sourceforge.net/modelparams.html)

Projeto original no Youtube, por Carlos Stein

[4](https://www.youtube.com/watch?v=xNXeQB2sDSI)

Universidade Tecnológica Federal do Paraná - Pato Branco, 2021

