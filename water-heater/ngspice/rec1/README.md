# Exercício de recuperação - Eletrônica A

Nome: Allan Ribeiro; RA: 2037807;

Programa utilizado: ngspice-35

### Índice:

* 1 - Descrição do problema

* 2 - Modelagem do diodo

* 3 - Modelagem da fonte

* 4 - Inclusão de bibliotecas

* 5 - Descrição do circuito

* 6 - Simulação

* 7 - Conclusão

* 8 - Referências

### Lista de arquivos:

* README.md - Arquivo de texto ASCII, descritor do projeto como um todo;

* rec1.cir - Arquivo de texto ASCII. Representação do circuito conforme sintaxe ngspice;

* D.lib - Biblioteca contendo modelos diversos de diodos;

* 1-1.png - Gráfico de tensão da fonte, led resistor, Vcc em 5V;

* 1-2.png - Gráfico de corrente no circuito, Vcc 5V;

* 1-3.png - Gráfico de tensão no led, Vcc em 5V;

* 2-1.png - Gráfico de tensão da fonte, led resistor, Vcc em 3.3V;

* 2-2.png - Gráfico de corrente no circuito, Vcc em 3.3V;

* 2-3.png - Gráfico de tensão no led, Vcc em 3.3V;

* 3-1.png - Gráfico de tensão da fonte, led resistor, 3.3 < Vcc < 5;

* 3-2.png - Gráfico de corrente no circuito, 3.3 < Vcc < 5;

* 3-3.png - Gráfico de tensão no led, 3.3 < Vcc < 5;

### 1 - Descrição do problema

Projetar e simular um circuito para acionamento de um LED,
considerando a saída de um microcontrolador.
 
	Vled = 2V; Iled = 50mA;
	Vcc = 5V; Igpio = 10mA;
 
2 - Refazer 1 considerando
 
	Vcc = 3.3V;

3 - Refazer 1 considerando

	3.3V < Vcc < 5V;

### 2 - Modelagem do diodo

O manual ngSPICE[1] aponta que as características DC de um diodo são
determinadas por dois parâmetros: A corrente de saturação IS, e o
coeficiente de emissão n. Além disso, características de resistência
Ôhmica podem ser estipuladas com RS, e tensão de junção com JV. Pela equação de Shockley para
diodos[2], tem-se:

	I = IS(e^(V/(n*VT)))

onde I é a corrente sobre o diodo, V é a tensão sobre o diodo, e VT é uma
função de temperatura e tensão, costante conhecida, a saber, 25.852mV[2]
para 27º celsius. Desta equação obtemos o respectivo coeficiente de
emissão necessário, a saber:

	n = V/(Vt*ln(I/Is+1)); 

Dado o limite de corrente da fonte como corrente no diodo, obtem-se

n ≃ 2.82;

O LED a ser simulado possui 2V de tensão direta, sem tensão reversa
prevista, e 50mA de corrente para ruptura. Está representado dentro da biblioteca "D.lib"
 
### 3 - Modelagem da Fonte

Para melhor observação do sinal, utilizar-se-á uma fonte, alternada, com
pico de 5 volts (amplitude), de 25Hz de frequência, e o
controle de corrente foi feito atráves de um resistor: para limitar a
corrente em 10mA, faz-se o equacionamento do circuito:

	Vcc - Ri - Vled = 0
	R = (Vcc-Vled)/i
 
Logo:

	Vcc = 5V, R = 0.3k ohms
	Vcc = 3.3V, R = 0.91k ohms

Para o valor de tensão entre 3.3V e 5V, utilizar-se-a a seguinte função:
 
	(Vcc - Vled)/R = 10
 
Sendo que
 
	Vcc = 4.15 + 0.85*sin(2*pi*25*t) (f = 25)
	Vled = 2.08+0.036*sin(2*pi*25*t) 
	(Vcc-Vled)/10m = R
 
logo, obtemos a expressão para a resistência
 
	R = (2.07 + sin(2*pi*25*t)*(0.886))

Observe que as fontes senoides não foram propostas no exercício original.
Foram apenas usadas para observar o efeito da variância de tensão sob o
led.


### 4 - Bibliotecas

Biblioteca contendo diversos modelos de diodos

	.include D.lib

### 5 - Descrição do circuito

![Circuito](imagens/circuito.png)

### 6 - Simulação

<details>
<summary><b>VCC = 5V</b></summary>

![Tensão da fonte, LED e resistor](imagens/1-1.png)
![Corrente no circuito](imagens/1-2.png)
![Tensão no LED](imagens/1-3.png)

</details>

<details>
<summary><b>VCC = 3.3V</b></summary>

![Tensão da fonte, LED e resistor](imagens/2-1.png)
![Corrente no circuito](imagens/2-2.png)
![Tensão no LED](imagens/2-3.png)

</details>

<details>
<summary><b>VCC entre 5V e 3.3V</b></summary>

![Tensão da fonte, LED e resistor](imagens/3-1.png)
![Corrente no circuito](imagens/3-2.png)
![Tensão no LED](imagens/3-3.png)

</details>

### 7 - Conclusão

Nos três casos a condução ocorre, fato este esperado. O mesmo circuito é
construído na prática em cursos de robótica como um primeiro contato dos
alunos. A necessidade do resistor é a mesma: limitar a corrente, e evitar
que o diodo queime.

Estipulando que o objetivo do circuito seja avaliar o comportamento do LED,
o circuito é funcional: Não existe corrente no circuito até que se
estabeleça a tensão de junção, de 2V, nos terminais do diodo. Como não foi
determinado uma tensão de ruptura reversa, nem corrente, o diodo foi
considerado ideal, e portanto, nunca conduz para qualquer tensão negativ,
ou possui qualquer resistência interna.

Por fim, corrente no resistor/potenciômetro é constante graças à variância
de sua resistência, de acordo com a tensão na fonte, conforme equacionamento
apresentado. Isso faz com que a corrente do circuito como um todo se
estabilize. Semelhante circuito pode ser construído na prática fazendo uso
de potênciômetros controlados por tensão.

### 8 - Referências

[Manual do ngSPICE](http://ngspice.sourceforge.net/docs/ngspice-manual.pdf)

[Equação de Shockley](https://en.wikipedia.org/wiki/Shockley_diode_equation)
 
### Universidade Tecnológica Federal do Paraná - Pato Branco, 2021
