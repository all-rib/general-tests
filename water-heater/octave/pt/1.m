% Burocracy (bleh)
clc;
clear;
close all;

% de carregamento do pacote
pkg load io;
pkg load signal;

% Incluir as funções para cada gráfico
source charts.m

## DOCUMENTAÇÃO:

## NOME:
#
#	PTC - Projeto de Controle de Temperatura

## AUTOR:
#
#	Allan Ribeiro

## VERSÃO:
#
#	v1.2

## LICENÇA:
#
#	Copyright © 2022 Free Software Foundation, Inc. Licença GPLv3+:
#	GNU GPL versão 3 ou posterior <https://gnu.org/licenses/gpl.html>.
#	Este é um software livre: você é livre para alterá-lo e redistribuí-lo.
#	NÃO HÁ GARANTIA, na medida permitida por lei.

## DESCRIÇÃO:
#
#	Este script foi criado como requisito do curso para Oficina de
#	Integração, durante o curso de Engenharia da Computação, Bacharel pela UTFPR.
#
#	O objetivo é controlar um sistema térmico, que consiste em um
#	aquecedor, e será medido constantemente e controlado a uma determinada
#	temperatura. O script serve como comparação entre
#	modelagens teóricas e práticas.

## MODELO TEÓRICO:
#
#	```
#
#	FIGURA 1 - Representação do sistema
#
#			Ext. ambiente
#			.................
#			.Água		.
#		+-------+-------+	.
#		|	.	|	.
#		Vcc	.	R-> Hi	. -> Ho
#		|	.	|	.
#		+-------+-------+	.
#			.		.
#			.................
#
#	A energia inserida acionada pela tensão sobre o resistor, e
#	a energia perdida, pois o sistema não é isolado.
#
#	```
#
#	O sistema pode ser concebido como um resistor, imerso em água, com
#	tensão controlada por uma fonte de tensão. A água tem isso
#	temperatura com sensor para manter o controle do estado do sistema.
#
#	A função é definida como o produto entre a resistência térmica
#	e Potência Térmica inserida no sistema:
#
#		Qi - Qo = m * c * (T-To)
#
#	Como o sistema percorre o tempo, tomamos sua variação
#
#		(Qi - Qo) / dt = m * c * (dT / dt)
#
#	Como "Q/dt" é a taxa de energia inserida no sistema, pode ser
#	reescrito:
#
#		Hi - Ho = m*c*(dT/dt)
#
#	Definindo
#
#		Rt = T/Ho; (eq.1)
#
#	como uma constante, conhecida como resistência térmica do sistema, e
#
#		C = m*c; (eq.2)
#
#	como a capacitância térmica:
#
#		Hi - T/Rt = C*(dT/dt)
#
#		Hi = C*(dT/dt) + T/Rt
#
#		Hi*Rt = (Rt*C)*(dT/dt) + T; (eq.3)
#
#	Com a transformada de Laplace, ela é definida como:
#
#		G(s) = T(s)/Hi(s) = Rt/(Rt*C*s + 1); (eq.4)
#
#
#	```
#
#	FIGURA 2: Função de transferência
#
#			+-------+
#			|	|
#		Hi(s)-> | G(s)	| -> T(s)
#			|	|
#			+-------+
#
#	O sistema é alimentado com energia de entrada e varia em temperatura.
#
#	```
#
#	Com Hi como potência térmica inserida no sistema, em Kcal, T como
#	função de temperatura (comportamento do sistema), em ºC, C como térmico
#	capacitância, C = m * c, com m como massa de água [Kg], c conforme específico
#	capacidade calorífica [Kcal*ºC/Kg], e Rt como resistência térmica.

% Capacitância térmica do sistema

m = 0.51; % massa de água
c = 1; % capacidade de calor específico, em joules
global C = m*c; % capacitância térmica

## RESISTÊNCIA ELÉTRICA
#
#	De acordo com a definição elétrica de potência, energia elétrica é a
#	taxa, no tempo, de energia elétrica transferida por um
#	o circuito. Pode ser escrito como:
#
#	P = W/t, onde P[W] é a potência, W[J] é a energia transferida e
#	t[s] é o tempo necessário.
#
#	Ao inserir a definição de corrente na equação, podemos
#	Escreva:
#
#		P = (W/Q) * (Q/t) = V*I;
#
#	Finalmente, usando a lei de Ohm (V = R*I), reescrevemos a equação como:
#
#		P = V^2/R;
#
#	E manipulando, nos é permitido encontrar a resistência teórica
#	do aquecedor
#
#		R = V^2/P; (eq.4)
#
#	Para este caso, consideraremos a tensão RMS (127V)

% Dados do aquecedor: Aquecedor RQ 127V, 770W
heater_voltage = 127;
heater_power = 770;
global heater_resistance = heater_voltage ^ 2 / heater_power;

## RESISTÊNCIA TÉRMICA
#
#	De acordo com a equação de resistência térmica (Lei de Fourier para um
#	dimensão), temos um fluxo de calor através de uma superfície (área normal),
#	dado por um gradiente de temperatura, como segue:
#
#		q = -k*dT/dx
#
#	Com q[W/m^2] como fluxo de calor, k[W/(K*m)] como a condutividade térmica do
#	material, e dT/dx[K/m] como a variação de temperatura por distância de
#	o material. Ao integrar em uma variação de tempo:
#
#		Q = -k*A*dT/dx
#
#	Onde Q[W] é o fluxo. Considerando o material utilizado, temos a
#	área de condutividade não como um plano, mas como um cilindro, que
#	equacione da seguinte forma:
#
#		Rt = L/(k*A)
#
#	Com: L[m] sendo a espessura do material, k[W/(K*m)] como a
#	condutividade térmica da amostra e A[m^2] é a seção transversal
#	área. Mas tomando o gradiente de temperatura e o fluxo de calor, através do mesmo
#	amostra, obtemos
#
#		Rt = L*T/(A*Hi*L) = T/Hi;
#
#	Que reitera a eq.1.
#

## CONSTANTES DE GANHO E TEMPO
#
#	Na forma de um sistema linear tipo 0:
#
#		Rt / (s*(Rt*C) + 1) = Kcc / (s*tau + 1)
#
#	Onde: "Kcc" é uma constante que define a variação de saída
#	de acordo com a variação de entrada, Rt;
#	"tau" é outra constante, que define o tempo que o sistema
#	precisa definir: Rt*C.
#
#	Esses valores foram determinados por observação: tau é o tempo
#	necessário para que o sistema atinja 63,21 % do seu valor final, e Kcc como o
#	razão entre a entrada de energia no sistema pela perda de energia do sistema quando
#	o sistema define.

function retval = avg(v) % Média de n números
	retval = sum(v)/length(v);
endfunction

function retval = time_constant(v) % Recuperação de constante de tempo com base em determinado sistema
	n = length(v);
	value = avg(v(n-20:n)); % Obter valor médio nos últimos 20 pontos, para servir como valor de configuração
	value = ceil(value*63.5/100); % Extrair 63,5% do valor
	i = 1;
	while(v(i) < value)
		i++;
	endwhile
	retval = i; % de extração constante de tempo
endfunction

#
#	A entrada prática do sistema é na verdade tensão, uma conversão
#	bloco é adicionado ao sistema:
#
#P = V^2/R;
#
#	Que é dado em joules. Um Joule é Kph = 2,39*10^-4 Kcal, e assim:
#
#	Hi = Kph*V^2/R;
#
#	```
#
#	FIGURA 3 : Função de transferência, tomando como entrada a tensão
#
#				+-------+		+-------+		+-------+
#				|	|		|	|		|	|
#		-> V(s) ->	| V^2/R | -> P(s) ->	| Kph	| -> Hi(s) ->	| G(s)	| -> T(s)
#				|	|		|	|		|	|
#				+-------+		+-------+		+-------+
#
#	O sistema varia em tensão, o que causará uma variação de potência,
#	assim, alterando a temperatura do sistema;
#
#	```
#
#	AS Kcc é, na verdade, a resistência térmica do sistema,
#
#	kcc = Rt = T/Hi; (eq.5)
#
#	tau = t, se g(t) = g(t->infinito)*0,63; (eq.6)
#

% de recuperação constante de ganho com base em determinado sistema

% Valor da etapa do sistema (em V)
global sv = 37;

% Constante de conversão de joules para cal
global Kph = 2.39*10^-4;

% Exportação de funções internas que devem se tornar globais

function retval = gain_constant(v)
	global heater_resistance;
	global Kph;
	global sv;

	global T = v(length(v))-v(1);
	global Hi = Kph*sv^2/heater_resistance;
	retval = T/Hi;
endfunction

% Primeiro bloco do sistema

## VALIDAÇÃO DO MODELO
#
#	A validação do modelo consiste em oferecer se seu
#	model é uma aproximação válida para um sistema prático. Desta forma,
#	plotar dados teóricos e práticos em sobreposição é um
#	jeito eficiente.
#

% modelo experimental

function retval = experiment
	% Importação de dados experimentais
	chart = csv2cell("experiment.csv");

	% Número de pontos no experiment
	n = length(chart)(1);

	% de itens de otimização
	aux1 = 1:n; % Intervalos de tempo
	aux2 = ones(n,1); % Vetor cheio de número um para operação mais rápida

	% Extração do valor de cada ponto
	b = cell2mat(chart(aux1, 3));

	% de adição de compensação
	retval = b - aux2*b(1);
endfunction

% Modelo teórico, aplicando conteúdos práticos

function retval = theory1 (tau, kcc, n)
	global Hi;
	global g1;

	% Especificação do estado do espaço
	A = [-1/tau];
	B = [kcc/tau];
	C = 1;
	D = 0;

	% Construção do numerador e denominador de Transf. Func.
	[num, den] = ss2tf(A, B, C, D);

	% Construção de Transf. Func.
	g1 = tf(num, den);

	g1 = g1*Hi;

	y = step(g1, 1:n);

	retval = y;
endfunction

% Modelo teórico, aplicando constantes teóricas

function retval = theory2 (c1, c2, n)
	global Hi;
	global g2;

	% Construção do numerador e denominador de Transf. Func.
	num = c1;
	den = [(c1 * c2), 1];

	g2 = tf(num, den);

	g2 = g2*Hi;

	y = step(g2, 1:n);

	retval = y;
endfunction

% Se você quiser regenerar os gráficos, defina CHARTS para 1
GENCHARTS = 1;

## RUÍDO NO MÉTODO DE MEDIÇÃO

#	Como nem as medidas e o método usado para medir são
#	ideais, a função adquirida está cheia de "picos", que é
#	indesejado. Um método de suavizar o ruído da função é trocar
#	cada vez amostra por sua média entre n amostras à frente, como
#	segue:
#
#	função-média(t) = soma(f(t))(de t a t+n)/n, onde n é o
#	número de pontos usados na média.
#
#	O gráfico 5 mostra a comparação entre a função original (vermelha) e a
#	função aproximada (verde)
#
#	<details>
#	<summary><b>Chart 5</b></summary>
#	![](5.png)
#	</details>
#
#	Nota: a menos que especificado de outra forma, a função aproximada será
#	usado.
#

% Aproxime uma função para a média de n amostras à frente
function retval = unoise(v, n)
	nv = length(v);
	if n < nv % o número de pontos usados na média
		limit = nv-n;
		for i = 1:limit
			retval(i) = avg(v(i:i+n));
		endfor
		% TODO: Média entre os últimos pontos, quando possível
		for j = i+1:nv
			retval(j) = v(j);
		endfor
	endif
endfunction

% Se você quiser que a função do experimento seja aproximada, defina USE AVERAGE para o
% número desejado de pontos a serem usados na média. A MEDIDA DE USO precisa ser
% menor que o número de pontos de tempo;

global USEAVERAGE = 16;

% Se você quiser ver a comparação entre o original e os vingadores, defina COMPARE AVERAGE para 1
COMPAREAVERAGE = 0;

if USEAVERAGE > 0
	exp2_data = experiment();
	global exp_data = unoise(exp2_data, USEAVERAGE);
	global n = length(exp_data);
	if COMPAREAVERAGE
		if GENCHARTS
			chart5(exp2_data, exp_data, n);
		endif
	endif
else
	global exp_data = experiment();
	global n = length(exp_data);
endif

global tau = time_constant(exp_data);
global kcc = gain_constant(exp_data);
global rt = kcc;

% Se você deseja validar modelos, que é comparar teóricos E reais
% model, defina VALIDATION para 1, para superposição de gráficos, ou EXPERIMENTAL e
% THEORICAL, para gráficos independentes para cada função. Definir apenas EXPERIMENTAL irá plotar
% gráfico EXPERIMENTAL, e somente THEORICAL, ou não definir nenhum, o gráfico THEORICAL
% será plotado.

VALIDATION = 0;
EXPERIMENTAL = 1;
THEORICAL = 1;

% Plotar gráficos experimentais e teóricos juntos
if VALIDATION
	global g1_data = theory1(tau, kcc, n);
	global g2_data = theory2(kcc, C, n);
	if GENCHARTS
		chart1(exp_data, g1_data, g2_data, n);
	endif
elseif EXPERIMENTAL & THEORICAL % Plote os dois modelos em gráficos diferentes
	global g1_data = theory1(tau, kcc, n);
	global g2_data = theory2(kcc, C, n);
	if GENCHARTS
		chart2(exp_data, n);
		chart3(g1_data, n);
		chart4(g2_data, n);
	endif
elseif EXPERIMENTAL % Plotar apenas modelo experimental
	if GENCHARTS
		chart2(exp_data, n);
	endif
else % Plotar apenas modelo teórico
	global g1_data = theory1(tau, kcc, n);
	global g2_data = theory2(kcc, C, n);
	if GENCHARTS
		chart3(g1_data, n);
		chart4(g2_data, n);
	endif
endif

##	MODELO ELÉTRICO EQUIVALENTE

#

##	ANÁLISE DE DADOS E ESCOLHA DO MODELO

#	O gráfico 1.png mostra a resposta do sistema para o sistema real, em
#	vermelho, a resposta teórica, usando as constantes práticas, em
#	verde, e a resposta teórica, usando constantes teóricas, em
#	azul. Para melhor visualização, as mesmas funções são plotadas, separadamente, em
#	quadros 2 (somente experimento), 3 (teórico, com constantes práticas),
#	e 4 (teórico, com constantes teóricas).
#
#	<details>
#	<summary><b>Chart 1</b></summary>
#	![](1.png)
#	</details>
#	<details>
#	<summary><b>Chart 2</b></summary>
#	![](2.png)
#	</details>
#	<details>
#	<summary><b>Chart 3</b></summary>
#	![](3.png)
#	</details>
#	<details>
#	<summary><b>Chart 4</b></summary>
#	![](4.png)
#	</details>
#
#	A curva mais próxima do sistema prático não é óbvia,
#	à primeira vista, e para esta ocasião, vamos calcular o
#	diferença para cada ponto no sistema:
#
#		delta(t) = |exp_data(t) - g1_data(t)|
#
#	Os resultados podem ser vistos no gráfico 6
#
#	<details>
#	<summary><b>Chart 6</b></summary>
#	![](6.png)
#	</details>
#
#	E, ainda assim, o resultado não é claro aos nossos olhos. Temos mais um
#	recurso, que serve para calcular o erro total acumulado no tempo: Integrando o erro para cada ponto simulado
#
#		integral(t) = delta(t), para t = 0
#
#		integral(t) = integral(t-1) + delta(t); t > 1
#
#	Os resultados podem ser vistos no gráfico 7.
#
#	<details>
#	<summary><b>Chart 7</b></summary>
#	![](7.png)
#	</details>
#

delta1(1) = abs(exp_data(1)-g1_data(1));
integ1(1) = delta1(1);
%integ1(1) = integ1(1); % Integral de integral
delta2(1) = abs(exp_data(1)-g2_data(1));
integ2(1) = delta2(1);
%integ2(1) = integr2(1); % Integral de integral
for i = 2:n
	delta1(i) = abs(exp_data(i)-g1_data(i));
	integ1(i) = integ1(i-1)+delta1(i);
	%integ1(i) = integ1(i-1)+integ1(i);
	delta2(i) = abs(exp_data(i)-g2_data(i));
	integ2(i) = integ2(i-1)+delta2(i);
	%integ2(i) = integ2(i-1)+integ2(i);
endfor

if GENCHARTS
	chart6(delta1, delta2, n);
	chart7(integ1, integ2, n);
	%chart7(integ1, integ2, n); % ver integral de integral
endif

#	No gráfico 6 vemos que, para aprox. 1800, G2 tem um maior
#	diferença do experimento, quase 4 graus, e consequentemente é integral (gráfico 7)
#	aumenta mais rápido; Através, a diferença G2 diminui em torno de 2700s,
#	ficando tão pequeno quanto 0, tendo um segundo pico em 4200, com 1 grau
#	de diferença. A integral também volta a crescer
#	,mas em menor imposto.
#
#	G1 fica mais perto do experimento até 1300s, em aprox. 1 grau, mas separe rapidamente
#	até 3000s, e gira para saltar em torno de 3 graus, definindo principalmente
#	em torno deste ponto.
#
#	No gráfico 7 vemos que a diferença da função G2 cresce
#	mais rápido para t < 1600, o valor integral final é maior para G1, mas
#	na maior parte do tempo G2 tem uma taxa de crescimento de erro menor do que G1.

#	Após analisar os gráficos, o sistema de controle ideal é o G2, pois a energia necessária para correção
#	será menor na maior parte do tempo, em que a soma de erros para
#	crescendo (após 2500s), e tem o menor erro para cada vez
#	após 1800.
#

#	EFEITOS DO ATUADOR NO LOOP DE CONTROLE

#	Existem várias maneiras de controlar a atividade do sistema, seja
#	usando MOSFETS, IGBT, fontes diretas e outros componentes. O IGBT
#	é um interruptor controlado de baixa tensão, proporcionando um interruptor rápido quando
#	polarizado na tensão especificada.
#
#	Por ser um switch, é possível usar como PWM (Pulse Width
#	Modulação), quando aplicado um AC, resultando em uma
#	conversão entre a alta tensão originada do original
#	tensão aplicada no aquecedor (127V), e uma baixa tensão confiável para
#	controle: tão baixo quanto 5V: a tensão média que este sistema gera é
#	dado da seguinte forma:
#
#	```
#
#	FIGURA N1
#
#		   ^
#		VCC|   +---+   +---+
#		   |   |   |   |   |
#		   |   |   |   |   |
#		   |   |   |   |   |
#		   |___|   |___|   |
#		   +---.---.--------> t(s)
#		   0   TX  T
#
#	O PWM é uma comutação entre VCC e 0, em um determinado ciclo de trabalho
#	(relação de temporização vcc por temporização 0v)
#
#	```
#
#		Toff = Tx
#		Ton = T-Tx
#
#		D = Ton/T;
#
#	Definindo integral como integral(V(t), To, Td) = integral de V(t), em
#	o intervalo de To (limite inferior) a Tf (limite superior):
#
#		Vavg = 1/T * integral(V(t), 0, T)
#		= 1/T * (integral(Vcc, 0, Ton) + integral(0, Ton, T))
#		= Vcc * Ton/T
#		= Vc * D; (eq. N2)
#
#	Como o modelo original varia de 0 a 37V, a máxima
#	tensão é definida. A tensão máxima de saída PWM é definida em
#	função do ciclo de trabalho, e resultando em uma forma de serra:
#
#		D = Vpwmin/Vtripico
#

#	FIGURA N1+1
#
#	A relação entre ciclos PWM, função dente de serra e controle
#	Voltagem

#

#	MÉTODO DE ATIVAÇÃO
#
##	RELÉ DE ESTADO SÓLIDO
#
#	O método selecionado de lidar com a corrente através do circuito foi
#	usando um Relé de Estado Sólido, que funciona baseado em um sinal PWM,
#	permitindo o fluxo de corrente em sua saída, neste caso, o
#	ficha de alimentação, com saída de 127V. A tensão prática do aquecedor
#	vê será:
#
#		Vef = Vp*sqrt(Ton/(2*T))
#
#	A relação entre os ciclos PWM e a tensão RMS não é linear,
#	embora> é quadrado. A relação muda, então, para:
#
#		Vef^2 = Vp^2 * Ton/(2*T)
#
#	Em termos práticos, a tensão da rede é de 127Vrms, a 60Hz de
#	frequência, resultando em 180V DC. Como o relé não suporta
#	60Hz, o valor terá apenas 6Hz, um pico de 10V, resultando em:
#
#		f = 6, portanto T = 1/6
#
#		Vef = 180*sqrt(1/6/(2*(1/6))) = 127,28 Vrms
#
#	O ganho total neste dispositivo é:
#
#		Kr = vout / vin = 127,28 ^ 2/10 = 1620,02
#

pwmin = 10; % tensão de entrada PWM
f = 6; % de frequência PWM
D = 1; % Ciclo de trabalho
T = 1/f; % período de PWM
Ton = D*T; % Período em que o PWM está em alta
Vef = 180*sqrt(Ton/(2*T)); % de tensão efetiva PWM
Kr = Vef ^ 2 / pwmin;

#
#	```
#
#	FIGURA
#
#	```
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#
#



%{
##	ÁREA SUPERFICIAL DO AQUECEDOR

#	O aquecedor consiste em uma resistência que circunda um plástico
#	estrutura. Possui pequenos laços, aumentando a área de contato (semelhante ao
#	resistências de chuveiro). O primeiro "loop" refere-se a esses pequenos loops.
#	O laço do laço refere-se ao círculo feito em torno da estrutura plástica
#
#	Os valores sufixados por * são medidos com o uso de um paquímetro (+-0,5mm
#	de precisão)
#
#	Raio (r)*
#
#	Wire circunference (wc): wc = 2*pi*r;
#
#	Raio do Loop (lr)*
#
#	Loop circunference (lc):
#
#lc = 2*pi*lr;
#
#	Área de um loop (al):
#
#	o = u*l*l;
#
#	Distância entre dois loops (dl)*
#
#	Raio do loop de um loop (lld)*
#
#	Circunferência do loop do loop (llc):
#
#	llc = 2*pi*llr;
#
#	Área de um loop loop (todos):
#
#	ils = seu*al;
#
#	Número do loop do loop (llln)*
#
#	Fio restante sem loop (nlw)*
#
#	Área total:
#
#	a = alll * lln + nlw;

##	ÁREA DE CONTATO AÉREO
#	Raio da Copa(cr):
#
#	Área de contato do ar
#
#	aca = pi * cr ^ 2;

##	ÁREA DE CONTATO DE AÇO
#
#	dado pelo volume de água dividido pela área da base do copo, que
#	dá a altura:
#
#h = wm/aca;
#
#	E devemos multiplicar pela circunferência do copo, mais a área do
#	fundo:
#
#	sca = 2 * pi * cr * h + aca;
#

% de condutividade térmica de aço (stc)[W/(mK)]

% de condutividade térmica do ar (atc)[W/(mK)]

% de precisão: +-0,5 mm

% TODO: Documente todo esse monte de contas

% Raio
r = 0,125;
% Wire circumference
wc = 2 * pi * r;
% Raio do Loop
lr = 1;
% Loop circumference
lc = 2*pi*lr;
% Área de um loop
o = lavar * para você;
% Distância entre dois loops
dl = 1,25;
% Raio do loop de um loop
lr = 7;
% Loop loop circumference
llc = 2*pi*llr;
% Área do loop de um loop
ilal = seu*al;
% Número do loop do loop: llln*
11n = 6;
% Fio restante sem loop:
nl = 20;
% Área total:
a = alll * lln + nlw;

% ÁREA DE CONTATO AÉREO
% de raio do copo
cr = 59;
% Área de contato do ar
aca = pi * cr ^ 2;

% Altura do copo
%h = wm/aca;
% stell área de contato
% sca = 2 * pi * cr * h + aca;

% de condutividade térmica de aço
st = 52;

% Condutividade térmica do ar
atc = 0,023;
%}


% Fonte:TDGC2-1,8KVA
% Sensor: ICEL MD-6510


#	MODELAGEM DE CONTROLE




##	REFERÊNCIAS
#
#	[] citações/ogata.bibtex
#
#	[2 - Manual](http://www.icel-manaus.com.br/download/MD-6150%20Manual.pdf), último acesso: 28-04-2022 [MM-DD-AAAA]
#
#	[3] https://carlroth.com/medias/SDB-1980-PT-PT.pdf?context=bWFzdGVyfHNlY3VyaXR5RGF0YXNoZWV0c3wzMzg1MzZ8YXBwbGljYXRpb24vcGRmfHNlY3VyaXR5RGF0YXNoZWV0cy9oMzgvaGJjLzg5NzExMjAxNDg1MTAucGRmfDM3NGQzNDdhNzZkNjUwY2I3OThjODExNDZmZTJkODIwMTNiNWNlMjIyNTkxZTNkYzBlMmY5MDY4Mjc5NjA4ZDc

##	A MEDIÇÃO

#	Para a medição do processo de aquecimento da água, foi usado o seguinte:
#	* Multímetro MD-6510 [1], Aquecedor(770W);
